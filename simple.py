import numpy as np
import matplotlib.pyplot as plt
import os
import struct
import math

class NeuralNetwork():
    def __init__(self, dimensions):
        np.random.seed(1)
        self.parameters = {}
        self.parameters['W1'] = np.random.randn(dimensions[1], dimensions[0])
        self.parameters['b1'] = np.zeros((dimensions[1], 1))
        self.parameters['W2'] = np.random.randn(dimensions[2], dimensions[1])
        self.parameters['b2'] = np.zeros((dimensions[2], 1))
        
    def sigmoid(self, Z):
        return 1/(1 + np.exp(-Z))
    
    def forward_progpagation(self, parameters, X):
        Z1 = np.dot(parameters['W1'], X) + parameters['b1']
        A1 = np.tanh(Z1)
        Z2 = np.dot(parameters['W2'], A1) + parameters['b2']
        A2 = self.sigmoid(Z2)
        self.cache = {'Z1': Z1, 'A1': A1, 'Z2': Z2, 'A2': A2}
        return A2
    
    def cost(self, prediction, Y):
        m = prediction.shape[1]
        cost = - ( 1/m )* np.sum(Y * np.log(prediction) + (1 - Y) * np.log(1 - prediction))
        return cost
    
    def activation_derivative(self, A, key):
        if key == 'sigmoid':
            derivative = A*(1 - A)
        elif key == 'tanh':
            derivative = 1 - A**2
        return derivative
    
    def backward_propagation(self, X, Y):
        dZ2 = self.cache['A2'] - Y
        m = X.shape[1]
        dW2 = 1/m * np.dot(dZ2, self.cache['A1'].T)
        db2 = 1/m * np.sum(dZ2, axis = 1, keepdims = True)
        dZ1 = np.dot(self.parameters['W2'].T, dZ2) * (1 - np.power(self.cache['A1'], 2))
        dW1 = 1/m * np.dot(dZ1, X.T)
        db1 = 1/m * np.sum(dZ1, axis = 1, keepdims = True)
        grads = {'dW1': dW1, 'db1': db1, 'dW2': dW2, 'db2': db2,}
        return grads
    
    def update_parameters(self, parameters, grads, learning_rate):
        parameters['W1'] = parameters['W1'] - learning_rate * grads['dW1']
        parameters['b1'] = parameters['b1'] - learning_rate * grads['db1']
        parameters['W2'] = parameters['W2'] - learning_rate * grads['dW2']
        parameters['b2'] = parameters['b2'] - learning_rate * grads['db2']
        return parameters
    
    def L_model_train(self, X, Y, learning_rate, iterations):
        self.costs = []
        self.numofepochs = []
        for i in range(iterations):
            prediction = self.forward_progpagation(self.parameters, X)
            cost = self.cost(prediction, Y)
            self.costs.append(cost)
            self.numofepochs.append(i)
            #if i % 100 == 0:
               # print(cost)
            grads = self.backward_propagation(X, Y)
            self.parameters = self.update_parameters(self.parameters, grads, learning_rate)

def load_mnist(path, kind='train'):
    """Load MNIST data from `path`"""
    labels_path = os.path.join(path, '%s-labels.idx1-ubyte' % kind)
    images_path = os.path.join(path, '%s-images.idx3-ubyte' % kind)
    with open(labels_path, 'rb') as lbpath:
        magic, n = struct.unpack('>II', lbpath.read(8))
        labels = np.fromfile(lbpath, dtype=np.uint8)
    with open(images_path, 'rb') as imgpath:
        magic, num, rows, cols = struct.unpack(">IIII", imgpath.read(16))
        images = np.fromfile(imgpath, dtype=np.uint8).reshape(len(labels), 784)
    return images, labels


#X_train, Y_train = load_mnist('', kind='train')
#print('Rows: %d, columns: %d' % (X_train.shape[0], X_train.shape[1]))
#X_test, Y_test = load_mnist('', kind='t10k')
#print('Rows: %d, columns: %d' % (X_test.shape[0], X_test.shape[1]))


dimensions = [3, 2, 2]
X = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 0], [1, 1, 1], [0, 1, 0], [1, 0, 0]]).T
Y = np.array([[0, 1, 1, 1, 0, 1, 0, 0], [0, 0, 0, 1, 1, 1, 0, 1]])
SNN = NeuralNetwork(dimensions)
SNN.L_model_train(X, Y, 0.1, 1000)
print()
cost = SNN.costs
numberofepochs = SNN.numofepochs
plt.plot(numberofepochs, cost)
plt.show
test = np.array([[0.666, 1, 0.001]]).T
print(SNN.forward_progpagation(SNN.parameters, test))
print('Parameters after training:')
print(SNN.parameters)
       
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        