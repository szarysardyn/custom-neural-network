"""
Created on Sat Dec  8 13:31:12 2018

@author: szary
"""

import numpy as np
import matplotlib.pyplot as plt


class NeuralNetwork():
    def __init__(self, dimensions):
        self.dimensions = dimensions
        np.random.seed(1)
        self.parameters = {}
        L = len(dimensions)
        for l in range(1, L):
            self.parameters['W' + str(l)] = np.random.randn(dimensions[l], dimensions[l - 1])*0.01
            self.parameters['b' + str(l)] = np.zeros((dimensions[l], 1))
    
    def linear_forward(self, A, W, b):
        Z = np.dot(W, A) + b
        linear_cache = (A, W, b)
        assert(Z.shape == (W.shape[0], A.shape[1]))
        return Z, linear_cache
    
    def sigmoid(self, Z):
        return 1/(1 + np.exp(Z)), 'sigmoid'

    def activation_forward(self, A_prev, W, b, activation_key):
        if activation_key == 'tanh':
            Z, linear_cache = self.linear_forward(A_prev, W, b)
            A = np.tanh(Z)
            linear_cache = linear_cache + (A,)
            activation_cache = 'tanh'
        elif activation_key == 'sigmoid':
            Z, linear_cache = self.linear_forward(A_prev, W, b)
            A, activation_cache = self.sigmoid(Z)
            linear_cache = linear_cache + (A,)
        cache = (linear_cache, activation_cache)
        return A, cache
    
    def forward_propagation(self, A):
        self.caches = []
        L = len(self.dimensions)
        for l in range(1, L-1):
            W = self.parameters['W' + str(l)]
            b = self.parameters['b' + str(l)]
            A_prev = A
            A, cache = self.activation_forward(A_prev, W, b, 'tanh')
            self.caches.append(cache)
            
        W = self.parameters['W' + str(L - 1)]
        b = self.parameters['b' + str(L - 1)]
        prediction, cache = self.activation_forward(A, W, b, 'sigmoid')
        self.caches.append(cache)
        return prediction
    
    def cost(self, prediction, Y):
        m = prediction.shape[1]
        cost = - 1/m * np.sum(Y * np.log(prediction) + (1 - Y) * np.log(1 - prediction))
        cost = np.squeeze(cost)
        return cost
    
    def activation_derivative(self, A, activation_key):
        if activation_key == 'tanh':
            derivative = 1 - np.power(A, 2)
        elif activation_key == 'sigmoid':
            derivative = A*(1 - A)
        return derivative
    
    def activation_backward(self, dA, cache):
        linear_cache, activation_key = cache
        A_prev, W, b, A = linear_cache
        derivative = self.activation_derivative(A, activation_key)
        dZ = dA * derivative
        m = dA.shape[1]
        dW = 1/m * np.dot(dZ, A.T)
        db = 1/m * np.sum(dZ, axis = 1, keepdims = True)
        dA_prev = np.dot(W.T, dZ)
        return dA_prev, dW, db
    
    def backward_propagation(self, prediction, Y, caches):
        grads = {}
        L = len(caches)
        dprediction = - np.divide(Y, prediction) + np.divide(1 - Y, 1 - prediction)
        current_cache = caches[L - 1]
        grads['dA' + str(L - 1)], grads['dW' + str(L)], grads['db' + str(L)] = self.activation_backward(dprediction, current_cache)
        for l in reversed(range(L - 1)):
            current_cache = caches[l]
            grads['dA' + str(l)], grads['dW' + str(l + 1)], grads['db' + str(l + 1)] = self.activation_backward(grads['dA' + str(l+1)], current_cache)
        return grads
    
    def update_parameters(self, grads, learning_rate):
        L = len(self.parameters) // 2
        for l in range(1, L):
            self.parameters['W' + str(l)] = self.parameters['W' + str(l)] - learning_rate * grads['dW' + str(l)]
            self.parameters['b' + str(l)] = self.parameters['b' + str(l)] - learning_rate * grads['db' + str(l)]
        return 

    def L_model_train(self, X, Y, learning_rate, iterations):
        costs = []
        numberofepochs = []
        for i in range(1, iterations):
            prediction = self.forward_propagation(X)
            cost = self.cost(prediction, Y)
            if i % 10 == 0:
                print(cost)
                costs.append(cost)
                numberofepochs.append(i)
            grads = self.backward_propagation(prediction, Y, self.caches)
            self.parameters = self.update_parameters(grads, learning_rate)
        plt.plot(numberofepochs, costs)
        plt.show()
            
        
    

dimensions = [3, 2, 1]
LNN = NeuralNetwork(dimensions)
X = np.array([[0, 0, 0], [0, 0, 1], [0, 1, 1], [1, 0, 1], [1, 1, 0], [1, 1, 1], [0, 1, 0], [1, 0, 0]]).T
Y = np.array([[0, 0, 1, 0, 1, 1, 1, 0]])
print('Neural Network starter parameters: ', LNN.parameters)

LNN.L_model_train(X, Y, 0.001, 1000)
print(LNN.parameters)









