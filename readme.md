# Welcome to my repo! 

During the last year or so, I've been learning some **machine learning/deep learning** concepts and this repo contain **projects** I've een working on.

* **simple.py** - one hidden-layer neural network initializer for predicting output number given a vector (for ex. **[1, 0, 1] --> 1**, **[0, 1, 1] --> 0**)

* **dense_initializer.py** - custom built dense layer neural network initializer

* **mnist.py** - neural network for classifying hand-written digits using mnist dataset


