#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 18:37:34 2019

@author: szary
"""

from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPool2D
from keras.utils import to_categorical
from keras.preprocessing import image

import numpy as np
np.random.seed(0)
import matplotlib.pyplot as plt 
#%matplotlib inline

def draw():
    plt.figure(figsize = (10, 10))
    for i in range(25):
        plt.subplot(5, 5, i + 1)
        plt.imshow(X_train[i+100], cmap = 'gray')
        plt.title('Class: {}'.format(Y_train[i+100]))
    plt.tight_layout()

(X_train, Y_train), (X_test, Y_test) = mnist.load_data()
X_train = X_train.reshape(-1, X_train.shape[1], X_train.shape[2], 1)
X_test = X_test.reshape(-1, X_test.shape[1], X_test.shape[2], 1)

if np.max(X_train) > 1:
    X_train = X_train / 255
if np.max(X_test) > 1:
    X_test = X_test / 255
if len(Y_train.shape) == 1:
    Y_train = to_categorical(Y_train, 10)
    Y_test = to_categorical(Y_test, 10)
    

input_shape = (X_train.shape[1], X_train.shape[2], 1)

model = Sequential([
        
        Conv2D(64, kernel_size = (3, 3), activation = 'relu', input_shape = input_shape),
        MaxPool2D(pool_size = (2, 2)),
        #Dropout(0.25),
        
        Conv2D(64, kernel_size = (5, 5), activation = 'relu'),
        MaxPool2D(pool_size = (2, 2)),
        #Dropout(0.25),
        
        Flatten(),
        
        Dense(512, activation = 'relu'),
        #Dropout(0.2),
        
        Dense(10, activation = 'softmax')
        
        ])
    
model.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])
print(model.summary())

model.fit(
        X_train, Y_train,
        batch_size = 128,
        epochs = 3,
        validation_data = (X_test, Y_test)
        )

score = model.evaluate(X_test, Y_test, verbose = 0)
print(score)







